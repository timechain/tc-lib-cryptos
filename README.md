# Timechain cryptos library

## Purpose of the library

This library provides various modules related to cryptocurrencies.

## Wallet module

The wallet module exposes functionalities to perform generic operations on cryptocurrencies.

As the objective of this package is to be lightweight and generic, no crypto support is directly implemented in this repository.

The wallet module exposes an interface to implement by every adapter to be compatible with the library.

### Usage

You can perform crypto operations directly on adapters from external libraries.

```js
import BitcoinAdapter from 'external-bitcoin-adapter-library'

BitcoinAdapter.generateRandomWallet().then((wallet) => {
  // We have our new wallet!
})

```

But what if you want to be able to perform operations dynamically, without having to care of fetching the good adapter?

```js
import { getAdapter, isRegistered, registerAdapter } from 'tc-lib-cryptos/dist/modules/wallet'
import BitcoinAdapter from 'external-bitcoin-adapter-library'
import { Wallet } from 'tc-lib-cryptos/dist/models'

// Before we can use the Bitcoin adapter from our library, we need to register it
registerAdapter(BitcoinAdapter)

async function generateRandomWallet (crypto: string): Promise<Wallet> {
  // Now we can dynamically fetch adapters from their symbol
  const adapter = getAdapter(crypto)
  return adapter.generateRandomWallet()
}

```
