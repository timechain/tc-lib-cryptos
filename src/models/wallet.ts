export default interface Wallet {
  crypto: string
  address: string
  publicKey: string
  privateKey: string
}
