import { BigNumber } from 'bignumber.js'

export default interface Transaction {
  hash: string
  date: Date
  fromAddress: string
  toAddress: string
  amount: BigNumber
  crypto: string
}
