import { BigNumber } from 'bignumber.js'

import { MockAdapter } from './mock-adapter'
import { registerAdapter, smallestUnitToCryptoUnit, cryptoUnitToSmallestUnit, isRegistered, registeredCryptos } from '../modules/wallet'

import { Wallet } from '../models'

registerAdapter(MockAdapter)

console.log('Conversion tests')
console.log(smallestUnitToCryptoUnit(new BigNumber(100), MockAdapter).toString(10))
console.log(smallestUnitToCryptoUnit(new BigNumber(100), 'MCK').toString(10))

console.log(cryptoUnitToSmallestUnit(new BigNumber(1), MockAdapter).toString(10))

console.log('Registred')
console.log(registeredCryptos())
console.log('BTC', isRegistered('BTC'))
console.log('MOCK', isRegistered('mOcK'))

MockAdapter.importWallet({}).catch((_error) => {
  console.log('Import wallet with missing info')
})

MockAdapter.importWallet({
  privateKey: 'abc',
  address: '4ddr355'
}).then((wallet) => {
  console.log('Import wallet with required info', wallet.address)
})
