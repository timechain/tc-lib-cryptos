import { BigNumber } from 'bignumber.js'
import { Wallet, Transaction } from '../models'
import { WalletAdapter, SendTransactionOptions } from '../modules/wallet'

const CRYPTO = 'MCK'

export const MockAdapter: WalletAdapter<Wallet> = {
  name: 'MockCoin',
  crypto: CRYPTO,
  decimals: 18,

  importWalletWithSeed: async function (seed: string): Promise<Wallet> {
    const data = await this.generateRandomWallet()
    return data.wallet
  },

  importWallet: async function (info: {
    publicKey?: string
    privateKey?: string
    address?: string
  }): Promise<Wallet> {
    if (!info.privateKey) throw new Error('Missing private key')

    return {
      crypto: this.crypto,
      address: info.address,
      publicKey: info.publicKey,
      privateKey: info.privateKey
    }
  },

  generateRandomWallet: async function (): Promise<{
    wallet: Wallet,
    seed: string
  }> {
    return {
      seed: 'mock-seed',
      wallet: {
        crypto: this.crypto,
        address: '0xMOCK123',
        publicKey: '0xPublicKey',
        privateKey: 'mock-private-key'
      }
    }
  },

  getBalance: async function (wallet: Partial<Wallet>): Promise<BigNumber> {
    return new BigNumber('12345678987654321')
  },

  sendTransaction: async function (options: SendTransactionOptions<Wallet>): Promise<Transaction> {
    return {
      hash: '0xTxId',
      date: new Date(),
      fromAddress: options.wallet.address,
      toAddress: options.recipientAddress,
      amount: options.amount,
      crypto: this.crypto
    }
  },

  getTransactionFees: async function (params: SendTransactionOptions<Wallet>): Promise<BigNumber> {
    return new BigNumber(42)
  },

  getTransactions: async function (wallet: Partial<Wallet>): Promise<Transaction[]> {
    const transactions: Transaction[] = []

    for (let i = 0; i < 20; i++) {
      transactions.push({
        hash: `0xTxId${i}`,
        date: new Date(),
        fromAddress: `0xAddress${Math.floor((Math.random() * 100000) + 1)}`,
        toAddress: `0xAddress${Math.floor((Math.random() * 100000) + 1)}`,
        amount: new BigNumber(Math.floor((Math.random() * 100000000000000) + 1)),
        crypto: this.crypto
      })
    }

    return transactions
  },

  validateAddress: function (address: string, options: object | null): boolean {
    return true
  }
}
