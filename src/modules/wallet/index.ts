import { BigNumber } from 'bignumber.js'

import Wallet from '../../models/wallet'
import Transaction from '../../models/transaction'

export type TransactionFeesPolicy = 'minimum'

export interface SendTransactionOptions<W extends Wallet> {
  wallet: W
  recipientAddress: string
  amount: BigNumber
  fees: TransactionFeesPolicy
}

export interface WalletAdapter<W extends Wallet> {
  /**
   * The symbol of the cryptocurrency supported by the adapter
   */
  crypto: string

  /**
   * The plain text name of the cryptocurrency supported by the adapter
   */
  name: string

  /**
   * Maximum decimal places from the natual unit to the smallest unit
   */
  decimals: number

  /**
   * Import a wallet by providing the seed.
   */
  importWalletWithSeed (seed: string): Promise<Wallet>

  /**
   * Import a wallet by providing information about keys.
   * The required inofrmation depend on each crypto.
   * Throws an error if the wallet could not be imported with the provided information.
   * @param info The required information to properly import the wallet.
   */
  importWallet (info: {
    publicKey?: string
    privateKey?: string
    address?: string
  }): Promise<Wallet>

  /**
   * Generates a random wallet
   */
  generateRandomWallet (): Promise<{
    wallet: W,
    seed: string
  }>

  /**
   * Fetches the current balace of a wallet
   * @param wallet The wallet
   */
  getBalance (wallet: Partial<W>): Promise<BigNumber>

  /**
   * Send crypto to another address
   * @param params The parameters to perform the transaction
   */
  sendTransaction (params: SendTransactionOptions<W>): Promise<Transaction>

  /**
   * Gets the fees for a transaction
   */
  getTransactionFees (params: SendTransactionOptions<W>): Promise<BigNumber>

  /**
   * Fetches all transactions concerning the wallet
   * @param wallet The wallet
   */
  getTransactions (wallet: Partial<W>): Promise<Transaction[]>

  /**
   * Check the valiity of an address.
   * @param address The address to check
   * @param options Additional options specific for a crypto
   */
  validateAddress (address: string, options: object | null): boolean
}

const adapters: {
  [crypto: string]: WalletAdapter<Wallet>
} = {}

/**
 * Gets the adapter for the specified cryptocurrency
 * @param crypto The crypto symbol
 */
export function getAdapter (crypto: string): WalletAdapter<Wallet> {
  const adapter = adapters[crypto.toLowerCase()]
  if (!adapter) throw new Error(`No adapter was registered for crypto ${crypto}`)
  return adapter
}

/**
 * Registers a new adapter for a cryptocurrency
 * @param adapter The adapter to register.
 */
export function registerAdapter<W extends Wallet> (adapter: WalletAdapter<W>) {
  adapters[adapter.crypto.toLowerCase()] = adapter
}

/**
 * Returns the list of registered cryptos
 */
export function registeredCryptos (): string[] {
  return Object.keys(adapters)
}

/**
 * Checks if a crypto is registered
 */
export function isRegistered (crypto: string): boolean {
  try {
    getAdapter(crypto)
    return true
  } catch (error) {
    return false
  }
}

/**
 * Converts an amount specified in the smallest unit of the crypto (satoshi, wei, ...)
 * to the default unit used by the cryptocurrency (bitcoin, ether, ...)
 * @param amount The amount to convert
 */
export function smallestUnitToCryptoUnit (amount: BigNumber, adapter: string | WalletAdapter<any>): BigNumber {
  if (typeof adapter === 'string') adapter = getAdapter(adapter as string)
  return amount.dividedBy(new BigNumber(10).exponentiatedBy(adapter.decimals))
}

/**
 * Converts an amount specified in natural unit (bitcoin, ether, ...)
 * to the smallest unit of the crypto (satoshi, wei, ...)
 * @param amount The amount to convert
 */
export function cryptoUnitToSmallestUnit (amount: BigNumber, adapter: string | WalletAdapter<any>): BigNumber {
  if (typeof adapter === 'string') adapter = getAdapter(adapter as string)
  return new BigNumber(10).exponentiatedBy(adapter.decimals).multipliedBy(amount)
}
