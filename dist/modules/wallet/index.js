"use strict";
exports.__esModule = true;
var bignumber_js_1 = require("bignumber.js");
var adapters = {};
/**
 * Gets the adapter for the specified cryptocurrency
 * @param crypto The crypto symbol
 */
function getAdapter(crypto) {
    var adapter = adapters[crypto.toLowerCase()];
    if (!adapter)
        throw new Error("No adapter was registered for crypto " + crypto);
    return adapter;
}
exports.getAdapter = getAdapter;
/**
 * Registers a new adapter for a cryptocurrency
 * @param adapter The adapter to register.
 */
function registerAdapter(adapter) {
    adapters[adapter.crypto.toLowerCase()] = adapter;
}
exports.registerAdapter = registerAdapter;
/**
 * Returns the list of registered cryptos
 */
function registeredCryptos() {
    return Object.keys(adapters);
}
exports.registeredCryptos = registeredCryptos;
/**
 * Checks if a crypto is registered
 */
function isRegistered(crypto) {
    try {
        getAdapter(crypto);
        return true;
    }
    catch (error) {
        return false;
    }
}
exports.isRegistered = isRegistered;
/**
 * Converts an amount specified in the smallest unit of the crypto (satoshi, wei, ...)
 * to the default unit used by the cryptocurrency (bitcoin, ether, ...)
 * @param amount The amount to convert
 */
function smallestUnitToCryptoUnit(amount, adapter) {
    if (typeof adapter === 'string')
        adapter = getAdapter(adapter);
    return amount.dividedBy(new bignumber_js_1.BigNumber(10).exponentiatedBy(adapter.decimals));
}
exports.smallestUnitToCryptoUnit = smallestUnitToCryptoUnit;
/**
 * Converts an amount specified in natural unit (bitcoin, ether, ...)
 * to the smallest unit of the crypto (satoshi, wei, ...)
 * @param amount The amount to convert
 */
function cryptoUnitToSmallestUnit(amount, adapter) {
    if (typeof adapter === 'string')
        adapter = getAdapter(adapter);
    return new bignumber_js_1.BigNumber(10).exponentiatedBy(adapter.decimals).multipliedBy(amount);
}
exports.cryptoUnitToSmallestUnit = cryptoUnitToSmallestUnit;
